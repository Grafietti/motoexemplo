const motoDAO = require("../dao/motoDAO");     

module.exports.selecionarMotos = () => {
    return motoDAO.selectMotos();
}

module.exports.selecionarMotoByID = (moto) => {
    return motoDAO.selectMotoById(moto);
}

module.exports.inserirMoto = (moto) => {
    return motoDAO.insertMoto(moto);
}

module.exports.alterarMoto = (moto) => {
    return motoDAO.updateMoto(moto);
}
module.exports.excluirMoto = (moto) => {
    return motoDAO.deleteMoto(moto);
}
