
--
-- Estrutura da tabela `motos`
--

CREATE TABLE `motos` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `Marca` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `Modelo` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Extraindo dados da tabela `motos`
--

INSERT INTO `motos` (`id`, `Marca`, `Modelo`, `created_at`, `updated_at`) VALUES
(1, 'Honda', 'Africa Twin', NULL, '2021-08-17 00:13:55'),
(2, 'Honda', 'CB 500', NULL, NULL),
(3, 'Honda', 'NX 750', '2021-08-14 00:49:07', '2021-08-14 00:49:07'),
(4, 'Kawa', 'Versys', '2021-08-14 00:52:05', '2021-08-14 00:52:05'),
(5, 'Kawa 2', 'Versys 2', '2021-08-14 00:54:45', '2021-08-14 00:54:45'),
(6, 'dede', 'deded', '2021-08-14 00:55:27', '2021-08-14 00:55:27');

--
-- Índices para tabelas despejadas
--

--
-- Índices para tabela `motos`
--
ALTER TABLE `motos`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de tabelas despejadas
--

--
-- AUTO_INCREMENT de tabela `motos`
--
ALTER TABLE `motos`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
