const db = require("./db.js");


exports.selectMotos = async function (){
    const conn = await db.connect();
    const [rows] = await conn.query('SELECT * FROM motos;');
    console.log(rows);
    return rows;
}

exports.selectMotoById = async function (moto){
    const conn = await db.connect();
    const sql = 'SELECT * FROM motos WHERE id=?;'
    const values = [moto.id];
    const [rows] = await conn.query(sql, values);
    return rows;
}

exports.insertMoto = async function (moto){
    const conn = await db.connect();
    const sql = 'INSERT INTO motos(Marca, Modelo) VALUES (?,?);';
    const values = [moto.marca, moto.modelo];
    return await conn.query(sql, values);
}

exports.updateMoto = async function (moto){
    const conn = await db.connect();
    const sql = 'UPDATE motos SET marca=?, modelo=? WHERE id=?';
    const values = [moto.marca, moto.modelo, moto.id];
    return await conn.query(sql, values);
}

exports.deleteMoto = async function (moto){
    const conn = await db.connect();
    const sql = 'DELETE FROM motos WHERE id=?';
    const values = [moto.id];
    return await conn.query(sql, values);
}



