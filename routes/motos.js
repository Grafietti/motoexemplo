var express = require('express');
var router = express.Router();

var motoController = require('./controller/motoController');
var motoDTO = require('./dto/motoDTO').motoDTO;

router.get('/selecionar', async function(req, res, next) {
  try {
    console.log('oi');
    const obj = await motoController.selecionarMotos();  
    res.status(200).json(obj);
  } catch(err){
    res.send(412,err);
  }
});

router.get('/selecionar/:id', async function(req, res, next) {
  motoDTO.id = req.params.id;
  try {
    console.log('oi2');
    const obj = await motoController.selecionarMotoByID(motoDTO);  
    res.status(200).json(obj);
  } catch(err){
    res.send(412,err);
  }
});


router.post('/inserir', async function(req, res, next) {
  motoDTO.marca = req.body.marca;
  motoDTO.modelo = req.body.modelo;  
  try{ 
    const obj = await motoController.inserirMoto(motoDTO);
    res.status(200).json(obj);
  } catch(err){
    res.send(412,err);
  }   
});

router.post('/alterar', async function(req, res, next) {
  motoDTO.id = req.body.id;
  motoDTO.marca = req.body.marca;
  motoDTO.modelo = req.body.modelo;  
  try{ 
    const obj = await motoController.alterarMoto(motoDTO);
    res.status(200).json(obj);
  } catch(err){
    res.send(412,err);
  }   
});

router.post('/deletar', async function(req, res, next) {
  motoDTO.id = req.body.id;  
  try{ 
    const obj = await motoController.excluirMoto(motoDTO);
    res.status(200).json(obj);
  } catch(err){
    res.send(412,err);
  }   
});

module.exports = router;